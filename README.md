CoreDataInMotionSample
==============

Extended example apps CoreDataInMotion book

## FailedBanksCD

This example is an extension of the RubyMotion Sample app called [Locations](https://github.com/HipByte/RubyMotionSamples/tree/master/Locations). 
However, it is also based on Ray Wenderlich's [Core Data Tutorial - Getting Started](http://www.raywenderlich.com/934/core-data-on-ios-5-tutorial-getting-started).

You will find the code relevent to each chapter in it's own branch.


